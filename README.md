# Ejercicio con GitLab

1. Clonar este repositorio: 
```shell
git clone https://gitlab.com/pcremades/prueba/
```

2. Agregar un archivo a la carpeta de trabajo.

3. Agregar el archivo al repositorio:
```shell
git add "nombre de archivo"
 ó
git add .
```

4. Registrar los cambios en el repositorio local.

```shell
git commit -m "Mensaje descriptivo de la actividad"
```
5. Subir los cambios al repositorio "origen" en GitLab

```shell
git push
```